import React from 'react';
import { UtilityThemeProvider, Box, Text } from 'react-native-design-utility';

export default function App() {
  return (
    <UtilityThemeProvider>
      <Box f={1} center>
        <Text>Jumaedi Ahsan</Text>
      </Box>
    </UtilityThemeProvider>
  );
}
